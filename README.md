# Brainiac Attack

I know, I know, the name is bad, it was supposed to be a place holder. This was an idea I had for a small game, nothing special.

## Basic Gameplay

User takes turns against an AI. On your **attack** turn, you must do two things: **signal** and **move**. The signal is a colour, either red, green, or blue, that you can choose every turn. It has no bearing on gameplay whatsoever, nor does gameplay affect the colours, but you'll see why it's important in a second. Your moves are **strike**, **fake**, or **reserve**. Then, knowing only your **signal** and not **action**, the AI chooses an action (defender actions: **counter**, **block**, **reserve**) . Both players reveal their move, and the round is judged. Judging is as follows

Attacker

 x  | Strike | Fake | Reserve
--- | ------ | ---- | -------
**Counter** | Tie | +1 | +1
**Block** | -1 | Tie | 
**Reserve** | +1 | 

Honestly this is a halfbaked plan


Ai tries to use your pattern. More unpredictable you are, the more it is.