var brain = require("brain");

var net = new brain.NeuralNetwork();

const str = (s) => { return JSON.stringify(s) };
// const score = (x) => { return Math.round(x*10) - 5 };
// const calc  = (x) => { return ((x*1) + 5.0) / 10 };
const score = (x) => { return x }
const format = (c, s) => { return { "input": colours[c], "output": [s*1] } };
const colours = {
	"red": {red: 1},
	"blue": {blue: 1},
	"green": {green: 1}
};

var data = [];

var stdin = process.openStdin();

process.stdout.write("=> ");
stdin.addListener("data", function(d) {
	const input = d.toString().trim().split(" ");

	data.push(format.apply(this, input));
	console.log(str(data));
	net.train(data);
	const guess = net.run(colours[input[0]]);

	console.log(` - you entered: ${str(input)}\n - brain guessed: ${guess[0]}`);
	process.stdout.write("=> ");
});

// var output = net.run(colours.blue);
// console.log(score(output[0]));